package it.unibo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration("myConfig")
public class MyConfig {
    @Value("${helloservice.uid}") private String helloServiceUid;
    public String getHelloServiceUid(){ return helloServiceUid; }
}
