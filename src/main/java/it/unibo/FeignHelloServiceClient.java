package it.unibo;

//import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${helloservice.uid}")
public interface FeignHelloServiceClient {
    @RequestMapping(method = RequestMethod.GET,
    value = "/v1/app/hello/{name}", consumes="text/plain")
    String getHelloMsg(@PathVariable("name") String name);
}
