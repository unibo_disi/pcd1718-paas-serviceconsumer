package it.unibo;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class HelloServiceClient {
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private MyConfig config;

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="5000")
            },
            fallbackMethod = "fallbackHello",
            threadPoolKey = "helloPool")
    public String getHelloMsg(String target) {
        List<ServiceInstance> ss = discoveryClient.getInstances(config.getHelloServiceUid());
        if(ss.size()==0) throw new RuntimeException("Cannot find any service");
        String uri = ss.get(0).getUri().toString()+"/v1/app/hello/"+target;
        return new RestTemplate()
                .exchange(uri, HttpMethod.GET, null, String.class)
                .getBody();
    }

    public String fallbackHello(String target){ return "[Fallback] HelloServiceClient, " + target; }
}