package it.unibo;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class HelloServiceClient2 {
    @Autowired RestTemplate rest;

    @Autowired
    private MyConfig config;

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="10000")
            },fallbackMethod = "fallbackHello",
            threadPoolKey = "helloPool"
    )
    public String getHelloMsg(String target){
        return rest
                .exchange("http://" + config.getHelloServiceUid() + "/v1/app/hello/"+target, HttpMethod.GET, null, String.class)
                .getBody();
        // Notice how we used the *service name* in the URL
    }

    public String fallbackHello(String target){ return "[Fallback] HelloServiceClient2, " + target; }

}