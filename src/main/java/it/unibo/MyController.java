package it.unibo;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/")
public class MyController {
    @Autowired HelloServiceClient helloService1;
    @Autowired HelloServiceClient2 helloService2;
    @Autowired FeignHelloServiceClient helloService3;

    @RequestMapping(value="/tell/{name}", method = RequestMethod.GET)
    public String hello(@PathVariable("name") String name){
        return "Asking hello-services: " + helloService1.getHelloMsg(name) + "\n" +
                helloService2.getHelloMsg(name) + "\n" +
                helloService3.getHelloMsg(name);
    }

    @RequestMapping(value="/1/{name}", method = RequestMethod.GET)
    public String hello1(@PathVariable("name") String name){
        return helloService1.getHelloMsg(name);
    }

    @RequestMapping(value="/2/{name}", method = RequestMethod.GET)
    public String hello2(@PathVariable("name") String name){
        return helloService2.getHelloMsg(name);
    }

    @RequestMapping(value="/3/{name}", method = RequestMethod.GET)
    public String hello3(@PathVariable("name") String name){
        return helloService3.getHelloMsg(name);
    }

    @RequestMapping(value="/4/{name}", method = RequestMethod.GET)
    public String hello4(@PathVariable("name") String name){
        return wrappedHello4Call(name);
    }

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="5000")
            },
            fallbackMethod = "fallbackHello",
            threadPoolKey = "helloPool"
    )
    public String wrappedHello4Call(String name){
        try  { Thread.sleep(10000); } catch (Exception e){}
        return "[Wrapped] Hello, " + name;
    }
    public String fallbackHello(String target){ return "[Fallback] Hello, " + target; }
}
